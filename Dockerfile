### STAGE 1:BUILD ###
FROM node:14.21-alpine AS build
WORKDIR /dist/src/app
RUN npm install -g @angular/cli
COPY ./package.json .
RUN npm install
COPY . .
RUN ng build

### STAGE 2:RUN ###
FROM nginx:latest AS ngi
COPY --from=build /dist/src/app/dist/neo-webphone /usr/share/nginx/html
COPY deploy/config/nginx.conf  /etc/nginx/conf.d/default.conf
EXPOSE 80