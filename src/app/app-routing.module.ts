import { AppComponent } from './app.component';
import { PermissionRequiredComponent } from './pages/permission-required/permission-required.component';
import { MainComponent } from './pages/main/main.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CallEndedComponent } from './pages/call-ended/call-ended.component';
import { MainPageComponent } from './pages/main-page/main-page.component';
// import { MainPageComponent } from './pages/main-page/main-page.component';

const routes: Routes = [
  { path: 'main', component: MainComponent },
  { path: 'permission-required', component: PermissionRequiredComponent },
  { path: 'call-ended', component: CallEndedComponent },
  { path: '', component: MainPageComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
