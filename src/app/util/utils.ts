export default class utils {
  static secondToTimeString(time: any) {
    const minutes = Math.floor(time / 60);
    const seconds = time - minutes * 60;
    return (
      this.str_pad_left(minutes, '0', 2) +
      ':' +
      this.str_pad_left(seconds, '0', 2)
    );
  }

  static str_pad_left(string: any, pad: any, length: any) {
    return (new Array(length + 1).join(pad) + string).slice(-length);
  }
}
