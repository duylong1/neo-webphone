import { OtpInputComponent } from './component/otp-input/otp-input.component';
import { PermissionRequiredComponent } from './pages/permission-required/permission-required.component';
import { MainComponent } from './pages/main/main.component';
// import { MainPageComponent } from './pages/main-page/main-page.component';
import { NgModule, Component, APP_INITIALIZER } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';

import { MediaDevicesCheckComponent } from './component/media-devices-check/media-devices-check.component';
import { MatDialogModule } from '@angular/material/dialog';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { MyHttpInterceptor } from './universal.interceptor';
import { CallEndedComponent } from './pages/call-ended/call-ended.component';
import { NgxOtpInputModule } from 'ngx-otp-input';
import { MainPageComponent } from './pages/main-page/main-page.component';

// export function init_app(appLoadService: AppConfigService) {
//   return () => appLoadService.setConfig();
// }

@NgModule({
  declarations: [
    AppComponent,
    MediaDevicesCheckComponent,
    CallEndedComponent,
    PermissionRequiredComponent,
    MainComponent,
    OtpInputComponent,
    MainPageComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    MatDialogModule,
    MatSlideToggleModule,
    MatProgressSpinnerModule,
    NgxOtpInputModule,
  ],
  providers: [
    // AppConfigService,
    // {
    //   provide: APP_INITIALIZER,
    //   useFactory: init_app,
    //   deps: [AppConfigService],
    //   multi: true
    // },
    { provide: HTTP_INTERCEPTORS, useClass: MyHttpInterceptor, multi: true },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
