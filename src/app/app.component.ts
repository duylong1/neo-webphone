import { CookieService } from './../lib/services/cookie.service';
import { lastValueFrom } from 'rxjs';
import { AuthService } from 'src/lib/api/auth.service';
import { ActivatedRoute, Router, UrlSegment } from '@angular/router';
import { Component, Inject, OnInit } from '@angular/core';
import { ENVIRONMENT } from 'src/main';
import * as _ from 'lodash';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'VPBank';
  constructor() {}
}
