import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-permission-required',
  templateUrl: './permission-required.component.html',
  styleUrls: ['./permission-required.component.scss']
})
export class PermissionRequiredComponent implements OnInit {

  constructor(private router : Router) { }

  ngOnInit() {

  }

  reCall(){
    this.router.navigate(['main'])
  }

}
