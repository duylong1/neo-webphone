import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import * as _ from 'lodash';
import { AuthService } from 'src/lib/api/auth.service';
import { CookieService } from 'src/lib/services/cookie.service';

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.scss'],
})
export class MainPageComponent implements OnInit {
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private auth: AuthService,
  ) {}

  ngOnInit(): void {
    this.route.queryParams.subscribe((param) => {
      if (!_.isEmpty(param)) {
        const token = param['token'];
        const session_id = param['session_id'];
        this.auth.setSession(token, session_id);
      }
    });
    this.router.navigate(['main']);
  }
}
