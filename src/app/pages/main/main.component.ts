import { CallLogService } from './../../../lib/api/calllog.service';
import { CallLogModel } from './../../../lib/models/calllog.model';
import { ContractModel } from './../../../lib/models/contract.model';
import {
  PULL_OVER_FORM_TYPE,
  NOTIFICATION,
} from './../../../lib/util/constant';
import { WebPhoneService } from './../../../lib/services/webphone.service';
import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FACING_MODE } from 'src/lib/util/constant';
import utils from 'src/lib/util/utils';
import {
  Inviter,
  InviterInviteOptions,
  SessionState,
  UserAgent,
  Web,
} from 'sip.js';
import { IncomingResponse } from 'sip.js/lib/core';
import { AuthService } from 'src/lib/api/auth.service';
import { ENVIRONMENT } from 'src/main';
import { CookieService } from 'src/lib/services/cookie.service';
import { ContractService } from 'src/lib/api/contract.service';
import * as _ from 'lodash';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss'],
})
export class MainComponent implements OnInit, OnDestroy {
  declare isMediaDevicesReady: boolean;
  declare constrainOption: any;
  declare localVideoStream: MediaStream;
  declare remoteVideoStream: MediaStream;

  declare isOpenPullOverBox: boolean;
  declare isShowWatingScreen: boolean;
  declare iShowNotification: boolean;

  declare notification: string;
  declare pullOverFormType: string;

  declare callingTime: number;
  declare isMicMuted: boolean;

  declare checkContractInterval: any;
  declare sessionContract: ContractModel;

  constructor(
    @Inject(ENVIRONMENT) private env: any,
    private router: Router,
    private webphone: WebPhoneService,
    private auth: AuthService,
    private cookie: CookieService,
    private contract: ContractService,
    private callLog: CallLogService
  ) {
    if (!this.auth.getToken()) {
      this.router.navigate(['permission-required']);
    }
  }

  ngOnDestroy(): void {
    if (this.localVideoStream) {
      const tracks = this.localVideoStream.getTracks();
      tracks.forEach((track) => track.stop());
    }
    clearInterval(this.checkContractInterval);
  }

  async ngOnInit() {
    // await lastValueFrom(this.auth.getTestSession()).then((res) => {
    //   if (res.data) {
    //     this.cookie.setCookie('token', res.data.access_token, 1);
    //     this.cookie.setCookie('session_id', res.data.session_id, 1);
    //     console.log(res.data.session_id);
    //   }
    //   setTimeout(async () => {
    //     this.isMicMuted = false;
        // this.pullOverFormType = PULL_OVER_FORM_TYPE.OTP;
        // this.isOpenPullOverBox = true;
    //     await this.setupMediaDevice();
    //     this.isMediaDevicesReady = true;
    //     this.webphone.initialize();
    //     this.webphone.initializeCompleted.subscribe(async (res) => {
    //       if (res) {
    //         await this.checkNewCallAvailable().then((isAvaialbe) => {
    //           if (isAvaialbe) {
    //             this.makeCall();
    //           } else {
    //             this.iShowNotification = true;
    //           }
    //         });
    //       }
    //     });
    //   });

    // });
    this.isMicMuted = false;
    await this.setupMediaDevice();
    this.isMediaDevicesReady = true;
    this.webphone.initialize();
    await this.checkNewCallAvailable().then((isAvailabe) => {
      if (isAvailabe) {
        this.webphone.initializeCompleted.subscribe(async (res) => {
          if (res) {
            this.makeCall();
          }
        });
      } else {
        this.iShowNotification = true;
      }
    });

  }

  async checkNewCallAvailable(): Promise<boolean> {
    if (!utils.checkCurrentTime()) {
      this.notification = NOTIFICATION.closed;
      return false;
    }
    // if(){
    // this.notification = NOTIFICATION.busy;
    // return false;
    // }
    return true;
  }

  async setupMediaDevice() {
    if (!navigator.mediaDevices) {
      this.setupMediaDeviceFailed();
      return;
    }

    this.constrainOption = {
      audio: true,
      video: {
        width: 1280,
        height: 720,
        facingMode: FACING_MODE.user,
      },
    };

    try {
      if (this.localVideoStream) {
        const tracks = this.localVideoStream.getTracks();
        tracks.forEach((track) => track.stop());
      }
      let stream = await navigator.mediaDevices.getUserMedia(
        this.constrainOption
      );
      if (stream) {
        this.localVideoStream = stream;
      }
    } catch (error) {
      this.setupMediaDeviceFailed();
    }
  }

  setupMediaDeviceFailed() {
    this.router.navigate(['permission-required']);
  }

  startTimer() {
    this.callingTime = 0;
    setInterval(() => {
      this.callingTime++;
    }, 1000);
  }

  get callTimeCount() {
    return utils.secondToTimeString(this.callingTime);
  }

  async makeCall() {
    this.isShowWatingScreen = true;
    setTimeout(() => {
      const ringBackAudio: any = document.getElementById('ringBackAudio');
      if (ringBackAudio) {
        ringBackAudio.play();
      }
    });
    const tenant_domain = this.env.sip_domain;
    const target = UserAgent.makeURI(
      `sip:${this.env.default_call_target}@${tenant_domain}`
    );
    if (!target) {
      return;
    }
    try {
      const constraints = {
        audio: true,
        video: {
          width: 1280,
          height: 720,
        },
      };

      const inviter = new Inviter(this.webphone.userAgent, target, {
        extraHeaders: [],
      });
      this.webphone.outGoingInviter = inviter;
      this.webphone.outGoingInviter.stateChange.addListener(
        (state: SessionState) => {
          switch (state) {
            case SessionState.Terminated:
              this.endCalls();
              break;
            default:
              break;
          }
        }
      );
      const _inviterInviteOptions: InviterInviteOptions = {
        sessionDescriptionHandlerOptions: {
          constraints,
        },
        requestDelegate: {
          // Khi cuộc gọi bắt đầu
          onProgress: async (response: IncomingResponse) => {
            const header = response.message.headers;
            const callLog: CallLogModel = {
              call_id: _.first(header['Call-ID'])?.parsed,
              callee: _.first(header['To'])?.parsed.uri.normal.user,
              caller: _.first(header['From'])?.parsed.uri.normal.user,
              call_log_session_id: _.first(header['X-Session-Id'])?.raw,
              direction: 'out',
              session_id: this.cookie.getCookie('session_id'),
            };
            this.callLog.setNewcallLog(callLog);
          },
          // Khi cuộc gọi được người nhận chấp nhận
          onAccept: (response: IncomingResponse) => {
            this.webphone.session = inviter;
            this.processCall();
          },
        },
        // requestOptions : {
        //   extraHeaders : ['??????']
        // }
      };

      await this.webphone.outGoingInviter.invite(_inviterInviteOptions);
    } catch (e) {
      console.error('ERROR in answer', e);
    }
  }

  async hangUpCall() {
    if (
      this.webphone.outGoingInviter.state == SessionState.Initial ||
      this.webphone.outGoingInviter.state == SessionState.Establishing
    ) {
      await this.webphone.outGoingInviter.cancel();
    } else {
      await this.webphone.session?.bye();
    }
    this.endCalls();
  }

  endCalls() {
    this.remoteVideoStream = new MediaStream();
    this.localVideoStream?.getTracks().forEach(function (track: any) {
      track.stop();
    });
    this.localVideoStream = new MediaStream();
    this.router.navigate(['call-ended']);
  }

  processCall() {
    const sessionDescriptionHandler =
      this.webphone.session.sessionDescriptionHandler;
    if (
      sessionDescriptionHandler &&
      sessionDescriptionHandler instanceof Web.SessionDescriptionHandler
    ) {
      const remoteStream = sessionDescriptionHandler.remoteMediaStream;

      setTimeout(() => {
        this.remoteVideoStream = remoteStream;
        // if (this.remoteVideoEl.nativeElement) {
        //   this.remoteVideoEl.nativeElement.playsInline = true;
        //   this.remoteVideoEl.nativeElement.srcObject = srcObject;
        // }
      });
    }
    const ringBackAudio: any = document.getElementById('ringBackAudio');
    if (ringBackAudio) {
      ringBackAudio.pause();
      ringBackAudio.currentTime = 0;
    }
    this.isShowWatingScreen = false;
    this.sessionStateListener();
    this.startTimer();
    this.checkContractInterval = setInterval(() => {
      this.contract.getContract().then((res: any) => {
        if (res) {
          this.pullOverFormType = PULL_OVER_FORM_TYPE.upgrade;
          this.isOpenPullOverBox = true;
          this.sessionContract = res.data;
          setTimeout(() => {
            clearInterval(this.checkContractInterval);
          });
        }
      });
    }, 5000);
  }

  async setStatusContract(status: boolean) {
    if (!status) {
      this.hangUpCall();
      this.router.navigate(['call-ended']);
      return;
    }
    await this.contract
      .updateContractStatus(this.sessionContract.id)
      .then(() => {
        this.pullOverFormType = PULL_OVER_FORM_TYPE.OTP;
      });
  }

  sessionStateListener() {
    this.webphone.session.stateChange.addListener((state: SessionState) => {
      switch (state) {
        case SessionState.Terminated:
          this.endCalls();
          break;
        default:
          break;
      }
    });
  }

  processCallAudio() {
    this.isMicMuted = !this.isMicMuted;
    this.localVideoStream.getAudioTracks()[0].enabled = !this.isMicMuted;
    var pc = this.webphone.session!.sessionDescriptionHandler!.peerConnection;
    const state = this;
    pc.getSenders().forEach((sender: any) => {
      if (sender.track.kind == 'audio') {
        sender.track.enabled = !state.isMicMuted;
      }
    });
  }

  onOTPSuccess(ev: boolean) {
    if (ev) {
      this.endCalls();
    }
  }
}
