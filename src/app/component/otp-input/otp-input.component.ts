import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { NgxOtpInputConfig } from 'ngx-otp-input/public-api';
import { OTPService } from 'src/lib/api/otp.service';
import utils from 'src/lib/util/utils';
import * as _ from 'lodash';

@Component({
  selector: 'app-otp-input',
  templateUrl: './otp-input.component.html',
  styleUrls: ['./otp-input.component.scss'],
})
export class OtpInputComponent implements OnInit {
  @Output() onOTPSuccess: EventEmitter<boolean> = new EventEmitter<boolean>();
  declare otpInputConfig: NgxOtpInputConfig;
  declare isOTPInvalid: boolean;
  declare otpTimeOut: number;
  declare countDownInterval: any;

  declare resentOTPTimeout: number;
  declare isResendOTPAvailable: boolean;

  constructor(private otp: OTPService) {}

  ngOnInit() {
    this.isResendOTPAvailable = true;
    this.resentOTPTimeout = 0;
    this.otpInputConfig = {
      otpLength: 6,
      numericInputMode: true,
      autofocus: true,
    };
    this.resetCountDownInterval();
  }

  async otpOnChange(input: Array<string>) {
    this.isOTPInvalid = false;
    if (input.filter((e: string) => !e).length > 0) {
      return;
    } else {
      const otpValue = input
        .reduce((accum, digit) => accum * 10 + parseInt(digit), 0)
        .toString();
      await this.otp.verifyOTP(otpValue).then((res) => {
        if (res) {
          this.onOTPSuccess.emit(true);
        } else {
          this.isOTPInvalid = false;
        }
      });
    }
  }

  resetCountDownInterval() {
    clearInterval(this.countDownInterval);
    this.otpTimeOut = 60; //second
    this.countDownInterval = setInterval(() => {
      if (this.otpTimeOut == 0) {
        return;
      }
      this.otpTimeOut = this.otpTimeOut - 1;
    }, 1000);
  }

  async resendOTP() {
    await this.otp.resendOTP().then(() => {
      this.isResendOTPAvailable = false;
      setTimeout(() => {
        this.isResendOTPAvailable = true;
      }, 60000);
      this.resentOTPTimeout = Date.now() + 60000;
      this.resetCountDownInterval();
    });
  }
}
