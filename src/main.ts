import { enableProdMode, InjectionToken } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';
import { overrideEnv } from './environments/overide-env';



// platformBrowserDynamic([{ provide: APP_CONFIG, useValue: config }])
//   .bootstrapModule(AppModule)
//   .catch((err) => console.error(err))

export const ENVIRONMENT = new InjectionToken<Record<string, unknown>>(
  'environment'
);

const main = async () => {
  overrideEnv(environment).then((dynamicEnv) => {
    if (dynamicEnv.production) {
      enableProdMode();
    }

    platformBrowserDynamic([{ provide: ENVIRONMENT, useValue: dynamicEnv }])
      .bootstrapModule(AppModule)
      .catch((err) => console.error(err));
  });
};

main();
