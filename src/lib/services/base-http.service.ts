import {
  HttpClient,
  HttpErrorResponse,
  HttpParams,
} from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { RequestMethod } from '../util/enum';
import { ENVIRONMENT } from 'src/main';

@Injectable({
  providedIn: 'root',
})
export class BaseService {
  constructor(
    @Inject(ENVIRONMENT) protected env: any,
    protected http: HttpClient
  ) {}

  request(
    method: RequestMethod,
    route: string,
    data?: any,
    params?: any,
    header?: any
  ): Observable<any> {
    return this.http.request(method, `${this.env.base_url}${route}`, {
      params: params,
      body: data,
      observe: 'body',
      headers: header,
    });
    // .pipe(catchError(this.handleError));
  }

  get(route: string, param: any = {}, header?: any): Observable<any> {
    let params = new HttpParams();
    if (param !== undefined) {
      Object.getOwnPropertyNames(param).forEach((key) => {
        params = params.set(key, param[key]);
      });
    }
    return this.http.get(`${this.env.base_url}${route}`, {
      headers: header,
      params,
    });
    // .pipe(catchError(this.handleError));
  }

  // Handle API errors
  handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` + `body was: ${error.message}`
      );
    }
    // return an observable with a user-facing error message
    return new Error(error.error.message);
  }
}
