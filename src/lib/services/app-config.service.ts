import { environment } from "./../../environments/environment";
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { from, map } from 'rxjs';
declare var window: any;

@Injectable()
export class AppConfigService {
  private declare configuration: any;
  constructor(private httpClient: HttpClient) {}

  setConfig() {
    this.httpClient
      .get<any>('/assets/app-config.json')
      .subscribe((config) => (this.configuration = config));
  }

  getConfig() {
    return this.configuration;
  }

  get environment() {
    return this.configuration;
  }
}
