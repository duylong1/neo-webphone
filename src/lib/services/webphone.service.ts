import { Router } from '@angular/router';
import { BehaviorSubject, lastValueFrom, Observable } from 'rxjs';
import { ExtensionService } from 'src/lib/api/extension.service';
import { Inject, Injectable } from '@angular/core';
import { Inviter, Registerer, UserAgent } from 'sip.js';
import { ENVIRONMENT } from 'src/main';

export class WebPhoneConfig {
  declare extension_number: string;
  declare extension_password: string;
  declare sip_domain: string;
  declare sip_server: string;
}

@Injectable({
  providedIn: 'root',
})
export class WebPhoneService {
  public declare isRemoteStreamReady: Observable<boolean>;

  private _initializeCompleted: BehaviorSubject<boolean>;
  public declare initializeCompleted: Observable<boolean>;

  private declare webPhoneConfig: WebPhoneConfig;
  private declare registerer: Registerer;
  declare userAgent: UserAgent;
  declare outGoingInviter: Inviter;

  public declare session: any;

  constructor(
    @Inject(ENVIRONMENT) private env: any,
    private extService: ExtensionService,
    private router: Router
  ) {
    this._initializeCompleted = new BehaviorSubject(false);
    this.initializeCompleted = this._initializeCompleted.asObservable();
  }

  async initialize() {
    await lastValueFrom(this.extService.getExtensionInfo())
      .then((res: any) => {
        if (res.data) {
          this.webPhoneConfig = {
            extension_number: res.data.ext_number,
            extension_password: res.data.ext_pass,
            // extension_password: '8269470531',
            sip_server: this.env.sip_server,
            sip_domain: this.env.sip_domain,
          };
        }
        this.registPortsip();
      })
      .catch((err) => {
        this.router.navigate(['permission-required']);
      });
  }

  async registPortsip() {
    if (!this.webPhoneConfig) {
      throw new Error('config is not defined');
    }
    const { extension_number, extension_password, sip_domain, sip_server } =
      this.webPhoneConfig;
    const uri = UserAgent.makeURI(`sip:${extension_number}@${sip_domain}`);

    const _userAgentOptions: any = {
      uri: uri,
      register: true,
      authorizationUsername: extension_number,
      authorizationPassword: extension_password,
      transportOptions: {
        server: sip_server,
      },
      logLevel: 'error',
      displayName: extension_number,
    };

    this.userAgent = new UserAgent(_userAgentOptions);

    this.registerer = new Registerer(this.userAgent, {});

    await this.userAgent.start().catch((err) => {
      console.error(err);
    });

    await this.registerer
      .register({
        requestDelegate: {
          onAccept: (resp) => {},
          onReject: (resp) => {},
        },
      })
      .catch((err) => {
        console.error(err);
      });

    this._initializeCompleted.next(true);
  }

  async makeCall(callNumber: string) {}

  private _processCalls() {}

  endCalls() {
    throw new Error('Method not implemented.');
  }
}
