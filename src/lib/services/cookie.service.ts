import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class CookieService {
  constructor() {}

  public getCookie(name: string) {
    let ca: Array<string> = document.cookie.split(';');
    let caLen: number = ca.length;
    let cookieName = `${name}=`;
    let c: string;

    for (let i: number = 0; i < caLen; i += 1) {
      c = ca[i].replace(/^\s+/g, '');
      if (c.indexOf(cookieName) == 0) {
        return c.substring(cookieName.length, c.length);
      }
    }
    return '';
  }

  public deleteCookie(name: string) {
    this.setCookie(name, '', -1);
  }

  public clearCookie() {
    const cookies = document.cookie.split(';');

    for (let i = 0; i < cookies.length; i++) {
      const cookie = cookies[i];
      const eqPos = cookie.indexOf('=');
      const name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
      document.cookie = name + '=;expires=Thu, 01 Jan 1970 00:00:00 GMT';
    }
  }

  public setCookie(name: string, value: string, expireMinutes: number) {
    var now = new Date();
    var time = now.getTime();
    var expireTime = time + expireMinutes * 1000 * 60;
    now.setTime(expireTime);
    document.cookie = `${name}=${value};expires=${now.toUTCString()};path=/`;

    // let d: Date = new Date();
    // d.setTime(d.getTime() + expireMinutes * 60 * 60 * 1000);
    // let expires: string = 'expires=' + d.toUTCString();
    // document.cookie = name + '=' + value + '; ' + expires + ';path=/';
  }
}
