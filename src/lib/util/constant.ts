export const FACING_MODE = {
  user: 'user',
  environment: 'environment',
};
export const PULL_OVER_FORM_TYPE = {
  upgrade: 'upgrade',
  OTP: 'OTP',
};

export const NOTIFICATION = {
  busy: `Xin lỗi Quý khách, hiện tại tất cả tổng đài viên đều đang bận, Quý khách vui lòng gọi lại sau ít phút. `,
  closed: `Ngoài thời gian sử dụng dịch vụ. Quý khách vui lòng thực hiện vào khoảng thời gian từ 8h00 - 22h00`,
};
