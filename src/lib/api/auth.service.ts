import { HttpClient } from '@angular/common/http';
import { lastValueFrom } from 'rxjs';
import { BaseService } from './../services/base-http.service';
import { CookieService } from '../services/cookie.service';
import { Inject, Injectable } from '@angular/core';
import { RequestMethod } from '../util/enum';
import { ENVIRONMENT } from 'src/main';

@Injectable({
  providedIn: 'root',
})
export class AuthService extends BaseService {
  constructor(
    protected override http: HttpClient,
    @Inject(ENVIRONMENT) protected override env: any,
    protected cookie: CookieService
  ) {
    super(env, http);
  }

  getTestSession() {
    return this.request(
      RequestMethod.POST,
      '/client/token',
      {
        ext_number: '0968997971',
      },
      {},
      {
        'x-vp-signature': 'test',
        'by-pass-signature': 'true',
      }
    );
  }

  setSession(token: string, session_id: string) {
    this.cookie.setCookie('token', token, 10);
    this.cookie.setCookie('session_id', session_id, 10);
  }

  getToken() {
    return this.cookie.getCookie('token');
  }

}
