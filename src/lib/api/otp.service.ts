import { lastValueFrom } from 'rxjs';
import { BaseService } from './../services/base-http.service';
import { Inject, Injectable } from '@angular/core';
import { RequestMethod } from '../util/enum';
import { CookieService } from '../services/cookie.service';
import { HttpClient } from '@angular/common/http';
import { ENVIRONMENT } from 'src/main';

@Injectable({
  providedIn: 'root',
})
export class OTPService extends BaseService {
  constructor(
    @Inject(ENVIRONMENT) protected override env: any,
    protected override http: HttpClient,
    private cookie: CookieService
  ) {
    super(env, http);
  }

  resendOTP() {
    const session_id = this.cookie.getCookie('session_id');
    return lastValueFrom(
      this.request(RequestMethod.POST, '/webrtc/sent-otp', {
        session_id,
      })
    );
  }

  verifyOTP(otp: string) {
    const session_id = this.cookie.getCookie('session_id');
    return lastValueFrom(
      this.request(RequestMethod.POST, '/webrtc/verify-otp', {
        session_id,
        otp,
      })
    );
  }


}
