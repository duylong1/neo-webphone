import { CookieService } from '../services/cookie.service';
import { Inject, Injectable } from '@angular/core';
import { BaseService } from '../services/base-http.service';
import { ENVIRONMENT } from 'src/main';

@Injectable({
  providedIn: 'root',
})
export class ExtensionService {
  constructor(
    private http: BaseService,
    private cookie: CookieService,
    @Inject(ENVIRONMENT) private env: any
  ) {}

  getExtensionInfo() {
    const session_id = this.cookie.getCookie('session_id');
    return this.http.get('/webrtc/extensions', {
      session_id,
    });
  }
}
