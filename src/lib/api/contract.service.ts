import { lastValueFrom } from 'rxjs';
import { RequestMethod } from './../util/enum';
import { BaseService } from './../services/base-http.service';
import { Inject, Injectable } from '@angular/core';
import { CookieService } from '../services/cookie.service';
import { HttpClient } from '@angular/common/http';
import { ENVIRONMENT } from 'src/main';

@Injectable({
  providedIn: 'root',
})
export class ContractService extends BaseService {
  constructor(
    @Inject(ENVIRONMENT) protected override env: any,
    protected override http: HttpClient,
    private cookie: CookieService
  ) {
    super(env, http);
  }

  async getContract() {
    const session_id = this.cookie.getCookie('session_id');
    return lastValueFrom(this.get(`/webrtc/contracts/${session_id}`));
  }

  async updateContractStatus(contractID: String) {
    return lastValueFrom(
      this.request(RequestMethod.PUT, `/webrtc/contracts/${contractID}`, {
        status: 'ok',
      })
    );
  }
}
