import { lastValueFrom } from "rxjs";
import { CallLogModel } from './../models/calllog.model';
import { BaseService } from './../services/base-http.service';
import { Inject, Injectable } from '@angular/core';
import { CookieService } from '../services/cookie.service';
import { RequestMethod } from '../util/enum';
import { HttpClient } from '@angular/common/http';
import { ENVIRONMENT } from 'src/main';

@Injectable({
  providedIn: 'root',
})
export class CallLogService extends BaseService {
  constructor(
    @Inject(ENVIRONMENT) protected override env: any,
    protected override http: HttpClient,
  ) {
    super(env, http);
  }

  setNewcallLog(callLog: CallLogModel) {
    return lastValueFrom(this.request(RequestMethod.POST, '/webrtc/calllogs', callLog));
  }
}
