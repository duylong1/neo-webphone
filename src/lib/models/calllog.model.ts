export class CallLogModel{
    declare callee : string;
    declare caller : string;
    declare direction : string;
    declare session_id : string ;
    declare call_id : string;
    declare call_log_session_id : string | undefined;
}