export class ContractModel {
  declare id: String;
  declare session_id: String;
  declare content: string;
  declare status: number;
  declare created_at: string;
  declare updated_at: string;
}
